# ha-rpicam

**rpicam** platform for homeassistant. Allows to control a raspberry pi camera through mqtt commands.
This is a third-party integration for homeassistant that provides further control from HA for a raspberry PI camera based on RPi-Cam-Web-Interface (https://elinux.org/RPi-Cam-Web-Interface).

It can be configured like a generic IP camera and supports the same configuration parameters:

    https://www.home-assistant.io/components/camera.generic/

Ensure the "rpicam" platform is specified instead. Configuration example:

      - platform: rpicam
        name: backyard
        still_image_url: http://192.168.1.12/cam.jpg
        username: <USER>
        password: !secret <USER_PASS>

This platform uses MQTT for interaction with the remote raspberry PI where an agent is configured and running (https://gitlab.com/gbus/rpi-cam-mqtt).

## What provides

Once all is set this platform provides methods for:

    - enable/disable the camera
    - take pictures/videos
    - enable/disable motion detection
    - change exposure mode
    - pan/tilt the camera

### Services
#### Camera services

- turn_on
- turn_off
- enable_motion_detection
- disable_motion_detection


#### rpicam specific services

- rpi_video_on
- rpi_video_off
- rpi_image
- rpi_exposure_mode
- rpi_set_pantilt

#### Services examples

Set exposure mode to "night":

    camera.rpi_exposure_mode:
    
    {
        "entity_id": "camera.backyard", 
        "mode": night
    }

Take a picture:

    camera.rpi_image:
    
    {
        "entity_id": "camera.backyard", 
        "type": image
    }

Take a 30 second video:

    camera.rpi_video_on:
    
    {
        "entity_id": "camera.backyard", 
        "type": video,
        "duration": 30
    }

Point the camera to view "gate":

    camera.rpi_set_pantilt:
    
    {
        "entity_id": "camera.backyard", 
        "view": "gate
    }

## What it dosn't provide
ha-rpicam doesn't send or receive media content through MQTT. This is a specific solution to only control the behaviour of the camera as an automation component under HA. Any commands triggering image/video capture, would be the same as pushing the camera web interface buttons. The captured media content would still be accessible as normal from the download area of the web interface itself.

## Automation examples

    automation:
    
      # Example 1
      alias: Set night exposure after sunset
      trigger:
        platform: sun
        event: sunset
      action:
        service: camera.rpi_exposure_mode
        data:
          entity_id: "camera.backyard"
          mode: "night"
          
      # Example 2
      alias: Disable indoor camera when family is back home
      trigger:
        platform: state
        entity_id: group.family
        from: not_home
        to: home
      action:
        service: camera.turn_off
        data:
          entity_id: "camera.living"
          
      alias: ... and enable it again when family has left home
      trigger:
        platform: state
        entity_id: group.family
        from: home
        to: not_home
      action:
        service: camera.turn_on
        data:
          entity_id: "camera.living"
          
      # Example 3
      alias: Reposition the camera when motion reported by PIR
      trigger:
      - entity_id: binary_sensor.gatepir
        platform: state
        to: 'on'
      action:
        service: camera.rpi_pantilt_view
        data:
          entity_id: "camera.backyard"
          view: "gate"
          